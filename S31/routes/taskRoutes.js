//ROUTE
//contains all the endpoints/routes for the application
//separate the routes such that "app.js" only contains information from the server

const express = require(`express`);
const router = express.Router(); //create a router instance that functions as a middleware and routing system. allows access to HTTP method middlewares that makes it easier to create routes for the application

const taskController = require(`../controllers/taskController`);

//routes
	//routes for getting all the tasks

router.get(`/`, (req, res)=>{
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController))
});

//route for creating a task
router.post(`/`, (req, res)=>{
	taskController.createTask(req.body).then(resultFromController => {
		res.send(resultFromController)
	});
});

//route for deleting a task
	//call out the routing component to register a brand new endpoint.
	//When integrating a path variable within the URI, you are also changing the behavior of the path from STATIC to DYANAMIC. 
	//2 Types of End points
	   //Static Route
	      //-> unchanging, fixed, constant,steady
	   //Dynamic Route
	   	 //-> interchangeable or NOT FIXED,

router.delete(`/:task`, (req, res) => {
	// taskController.deleteTask();
	//identify the task to be executed within this enpoint
	//call out the proper function for this route and identify the source/provider of the function.
	//DETERMINE wether the value inside the path variable is transmitted to the sever.

	console.log(req.params.task);
	// res.send(`This is from delete`);
	//place the value of the path variable inside its own container. 
	let taskId = req.params.task;

	// res.send('Hello from delete'); //this is only to check if the setup is correct

	//retrieve the identity of the task by inserting the ObjectId of the resource. 
	//make sure to pass down the identity of the task using the proper reference (objectID), however this time we are going to include the information as a path variable.
	//Path variable -> this will allow us to insert data within the scope of the URL. 
		//what is variable ? -> container, storage of information
    //When is a path variable useful? 
      //-> when iserting only a single piece of information.
      //-> this is also usefull when passing down information to REST API method that does NOT include a BODY section like 'GET'.

    //upon executing this method a promise will be initialized so we need to handle the result of the promise in our route module.

	taskController.deleteTask(taskId).then(resultOfTask => res.send(resultOfTask));
});

//Route for Updating task status
	//Lets create a dynamic endpoint for this brand new route.

router.put(`/:task`, (req, res) => {
	//check if you are able to acquire the values inside the path variables.
	// taskController.deleteTask();
	// console.log(req.params.task); //check if the value inside the parameters of the route is properly transmitted to the server.
	// res.send(`This is from delete`);
	let taskId = req.params.task; //this is declared to simplify the means of calling out the value of the path variable key.

	//identify the business logic behind this task inside the controller module.
    //call the intended controller to execute the process make sure to identify the provider/source of the function.
    //afte invoking the controller method handle the outcome.

	taskController.taskCompleted(taskId).then(resultOfTask => res.send(resultOfTask));
});

//route for updating status (complete to pending)
	//will be a counter procedure for the previous task

router.put(`/:task/pending`, (req, res) => {
	let id = req.params.task;
	// console.log(req.params);
	//declare the business logic aspect of brand new task in app
	//invoke the task to execute in the route

	taskController.taskPending(id).then(outcome => {
		res.send(outcome);
	});
});

//ACTIVITY

	//GET one task
	router.get(`/:id`, (req,res)=>{
		// res.send(`This is for get`);
		console.log(req.params.id);

		taskController.retrieveId(req.params.id).then(result=>{
			res.send(result);
		})
	})

	//UPDATE task name
	router.put(`/:id/update-name`, (req,res) => {
		// res.send(`You connected to update-name method`);
		console.log(req.params.id);
		console.log(req.body.name);

		if(req.body.name !== ""){
			taskController.updateName(req.params.id, req.body.name).then(result=>{
				res.send(result);
			})
		}
		else{
			res.send(`Task name should not be empty`);
		}
	});



//END OF ACTIVITY

//export it. module exports to export the router object to use in the "app.js"

module.exports = router;
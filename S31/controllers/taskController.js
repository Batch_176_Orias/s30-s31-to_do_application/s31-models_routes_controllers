//CONTROLLER
//controllerts contains the function and business logic of the express js application

const Task = require(`../models/task`);

//section - controllerts

//create

	module.exports.createTask = (requestBody) => {
		let newTask = new Task({
			name: requestBody.name
		});

		return newTask.save().then((task, error)=>{
			if(error){
				return false;
			}
			else{
				return task;
			}
		})
	};

//retrieve

	module.exports.getAllTasks = () => {
		//The "return" statement, returns the result of the Mongoose method "find" back to the "taskRoute.js" file which invokes this function the "/tasks" routes is accessed
		return Task.find({}).then(result => {

			//The "return" statement returns the result of the MongoDB query to the "result" parameter defined in the "then" method
			return result
		}).catch(error => {
			return `Record not found`
		})

	};

//update
	//1. Change status of Task. pending -> 'completed'.
	//we will reference the document using it's ID field.

	module.exports.taskCompleted = (taskId) => {
		//Query/search for the desired task to update.
		//The "findById" mongoose method will look for a resource (task) which matches the ID from the URL of the request.
		//upon performing this method inside our collection, a new promise will be instantiated, so we need to be able to handle the possible outcome of that promise.
		// return Task.findById(taskId).then((found, error) => 
			//describe how were going to handle the outcome of the promise using a selection control structure.
			//error -> rejected state of the promise
			//process the document found from the collection and change the status from pending to completed

		return Task.findById(taskId).then((success, fail) => {
			if (success) {
				console.log(success);
				//modify the status of returned document to completed
				success.status = `Completed`;
				//save the new changes inside the db
				//upon saving the new changes for the document, a 2nd promise will be initialized
				//chain a thenable expression upon performing a save() method into the return document
				// return `Record ${taskId} found`;
				return success.save().then((updatedTask, taskErr)=>{
					//catch the state of promise to identify a specific response
					if (updatedTask) {
						return `Task ${success.name} has been successfully updated`;
					} 
					else {
						//return the error
						return taskErr;
					}
				})
				//call the parameter that describes the result of query when successful
			} 
			else {
				return `Record ${taskId} not found`;
			}
		});
	};

	//change the status from completed to pending

	module.exports.taskPending = (userInput) => {
		//expose new component
		//search the database for the user input
		//findById mongoose method -> will run a search query inside the db using id field as its reference
		//since performing this method will have 2 possible outcomes, chain a then expression to handle possible states of the promise
		return Task.findById(userInput).then((res, err)=>{
			//handle and catch the state of promise
			if(res){
				//process the result of the query and extract the property to modify its value
				// res.status = `Pending`;
				// return `Record found`;

				//save the new change in the document

					return res.save().then((updated, error) => {
						if (updated) {
							return `Task ${updated.name} updated`;
						} 
						else {
							return `Error upon updating task ${updated.name}`;
						}
					});
					/*if (res.status === "pending") {
						return `this task is pending`;
					} else {
						return `this task is not pending`;
					}*/
			}
			else{
				return `Record not found`;
			}
		});
	};

//delete

	//remove an existing record
		//expose the data across other modules in app to become reusable
		//does it need an input from user
	module.exports.deleteTask = (taskId) => {
		//how will the data will be processed to execute the task
		//select wc mongoose method will be used to acquire the desired end goql
		//mongoose -> it provides an interface and methods to be able to manipulate the resources found inside the mongoDB atlas
		//to identify the location in wc the function will be executed append the model name
		return Task.findByIdAndRemove(taskId).then((fulfilled, rejected) => {
			//identify how to act accrdg to outcome
			if (fulfilled) {
				return `Task has been removed`;
			} 
			else {
				return `Task unsuccessfully removed`;
			}
		});
	};

	//ACTIVITY

	//GET one task
	module.exports.retrieveId = (id) => {
		return Task.findById(id).then((success, fail) => {
			if (success) {
				return `Record "${id}: ${success.name}" found`;
			} 
			else {
				return `Record ${id} not found`;
			}
		});
	};

	//UPDATE task name

	module.exports.updateName = (id, name) => {
		return Task.findById(id).then((success, fail)=>{
			if (success) {
				success.name = name;
				return success.save().then((updated, failed)=>{
					if (updated) {
						return `Record "${id}" found and will be updated to: ${success.name}`;
					} else {
						return `Error in updating`;
					}
				});
			} 
			else {
				return `Record ${id} not found`;
			}
		});
	};

//END OF ACTIVITY
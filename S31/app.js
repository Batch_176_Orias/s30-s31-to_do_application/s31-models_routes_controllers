/*mini act
	setup a basic express JS server
*/

const express = require("express");
const mongoose = require("mongoose");
const port = 4000;
const application = express();
const db = mongoose.connection;
const taskRoute = require("./routes/taskRoutes");

application.use(express.json()); //will accept requests from api
application.use(express.urlencoded({extended: true})); //will accept requests coming from form

mongoose.connect(`mongodb+srv://SeedMemes:admin123@cluster0.o7qjf.mongodb.net/toDo176?retryWrites=true&w=majority`,{

	useNewUrlParser: true,
	useUnifiedTopology: true

});


db.on(`error`, console.error.bind(console, `Connection Error`));
db.once(`open`, ()=>{
	console.log(`Now connected to MongoDB`);
});

//add task route
application.use(`/tasks`, taskRoute);

application.listen(port, ()=>{
	console.log(`Server is now running at port ${port}`);
});
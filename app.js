//1 dependencies and modules
	let server = require("express");
	let mongoose = require("mongoose");

//2 server setup
	//establish a connection
	let app = server();
	//define path/address
	let port = 3000;
	//assign 4000 to avoid conflict in the future w frontend application that run on port 3000

//3 database connection
	//connect to mongodb atlas
	mongoose.connect("mongodb+srv://SeedMemes:admin123@cluster0.o7qjf.mongodb.net/toDo176?retryWrites=true&w=majority", {

		//options to add to avoid deprecation warnings bc of mongoose update
		useNewUrlParser: true,
		useUnifiedTopology: true

	});
	//create notifs if connection to db is success or failure
	let db = mongoose.connection;
	//add an on() method to show if connection is successful or fail in both terminal and browser
	db.on(`error`, console.error.bind(console,`Connection Error`));
	//once connection is open and successful, output a message in the terminal
	db.once(`open`, ()=>{
		console.warn(`Server running at port ${port}`)
	});

	//middleware - in expressjs context, are methods, functions that acts and adds features to application
	app.use(server.json());

	//schema
	//before creating docus fr api to save into db, first have to determine the structure of docus to be written in db. This is to ensure the consistency of docus and avoid future errors

	//schema acts as a blueprint for data/document

	//schema() constructor fr mongoose to create a new schema object
	let taskSchema = new mongoose.Schema({
		/*
			Define fields for docus
			Will be able to determine the appropriate data type of values
		*/
		name: String,
		status: String

	});

	//mongoose model
	/*
		models are used to connect api to corresponding collection in db. It is a representation of collection

		models use schemas to create object that correspond to schema. by default, when creating collection fr model, the collection name is pluralized

		mongoose.model(<nameOfCollectionInAtlas>, <schemaToFollow>)
	*/

	//Task model

	let Task = mongoose.model(`task`, taskSchema);

	//post route to create new task
	app.post(`/tasks`, (req, res)=>{

		//when creating a new post/put or any route that requires data fr the client, first console log req.body or any part of the request that contains the data
		// console.log(req.body);

		//creating new task docu by using the constructor of Task model. Constructor should follow the schema of model.
		let newTask = new Task({
			name: req.body.name,
			status: req.body.status
		});

		//.save() method fr an object created by a model
		//save() method will allow to save docu by connecting to collection via model

		//save() has 2 approaches: 
		//add an anonymous function to handle the created docu or error
		//add .then() chain wc will allow to handle errors and create docus in separate functions
		/*newTask.save((error, savedTask)=>{
			if(error){
				res.send(`Error in creation`);
			}
			else{
				res.send(savedTask);
			}
		})*/

		//.then() and .catch() chain
		//.then() is used ti handle the proper result/returned value of a function. If the function properly returns a value, there can be a separate function to handle it.
		//.catch() is used to handle the error from the use of a function. If an error occurs, error can be handled separately
		newTask.save()
		.then(result => res.send({message: `Successful`}))
		.catch(error => res.send({message: `Error in creation`}));

	});


	//get method request to retrieve all task docues fr collection

	app.get(`/tasks`, (req, res)=>{
		//using mongoose, first access the model of collection
		//mongoose - db.tasks
		//model.find() in mongoose is similar to mongoDb's db.collection.find()
		Task.find({})
		.then(result => {
			res.send(result)
		})
		.catch(err => {
			res.send(err);
		})
	});

	//Sample model


	let sampleSchema = new mongoose.Schema({
		name: String,
		isActive: Boolean
	});

	let Sample = mongoose.model(`samples`, sampleSchema);
	app.post(`/samples`, (req, res)=>{
		let newSample = new Sample({
			name: req.body.name,
			isActive: req.body.isActive
		})

		newSample.save((error, savedSample)=>{
			if(error){
				res.send(error);
			}
			else{
				res.send(savedSample);
			}
		})
	});

	app.get(`/samples`, (req, res)=>{
			//using mongoose, first access the model of collection
			//mongoose - db.tasks
			//model.find() in mongoose is similar to mongoDb's db.collection.find()
			Sample.find({})
			.then(result => {
				res.send(result)
			})
			.catch(err => {
				res.send(err);
			})
		});

	//manggagamit model

	let manggagamitSchema = new mongoose.Schema({
		username: String,
		isAdmin: Boolean
	});

	let Manggagamit = mongoose.model(`manggagamit`, manggagamitSchema);
	app.post(`/mgaManggagamit`, (req, res)=>{
		let newManggagamit = new Manggagamit({
			username: req.body.username,
			isAdmin: req.body.isAdmin
		});

		newManggagamit.save((savedManggagamit, error)=>{
			if (error) {
				res.send(error);
			} else {
				res.send(savedManggagamit);
			}
		})
	});

	//get db's credential from user

	//USERS

	//first way to do it

	let UserSchema = new mongoose.Schema({
		username: String,
		password: String
	});

	let User = mongoose.model(`user`, UserSchema);

	app.post(`/users`, (req, res)=>{
		let newUser = new User({
			username: req.body.username,
			password: req.body.password
		});

		newUser.save((error, result)=>{
			if (error) {
				res.send(`Record can not be added`);
			} else {
				res.send(`Record added`);
			}
		})
		
	})

	app.get(`/users`, (req, res)=>{
		User.find({})
		.then(result => res.send(result))
		.catch(error => res.send(error));
	})

	//other way to do it

	/*let userSchema = new mongoose.Schema({
		username: String,
		password: String
	});

	let User = mongoose.model(`users`, userSchema);
	app.post(`/users`, (req, res)=>{
		let newUser = new User({
			username: req.body.username,
			password: req.body.password
		})

		newUser.save()
				.then(result => res.send(`Record added`))
				.catch(error => res.send(`Record error`));
	});

	app.get(`/users`, (req, res)=>{
		User.find({}, (error, result)=>{
			if (error) {
				res.send(`Record can not be loaded`);
			} else {
				res.send(result);
			}
		});
	});
*/

//4 entry point response
	//bind connection to designated port
	app.listen(port, ()=>{
		console.log(`Server running at ${port}`);
	})